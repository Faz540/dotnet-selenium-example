using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace dotnet_selenium_example
{
    class SeleniumSetMethods
    {
        public static void GoToURL(string url)
        {
            PropertiesCollection.driver.Navigate().GoToUrl(url);
        }
        public static void EnterText(string selector, string value)
        {
            PropertiesCollection.driver.FindElement(By.CssSelector(selector)).SendKeys(value);

        }

        public static void Click(string selector)
        {
            PropertiesCollection.driver.FindElement(By.CssSelector(selector)).Click();
        }

        // Dropdown Methods:
        public static void SelectTextValueFromDropDown(string selector, string dropdownText)
        {
            new SelectElement(PropertiesCollection.driver.FindElement(By.CssSelector(selector))).SelectByText(dropdownText);

        }

        public static void SelectValueFromDropDown(string selector, string dropdownValue)
        {
            new SelectElement(PropertiesCollection.driver.FindElement(By.CssSelector(selector))).SelectByValue(dropdownValue);

        }

        public static void SelectIndexValueFromDropDown(string selector, int index)
        {
            new SelectElement(PropertiesCollection.driver.FindElement(By.CssSelector(selector))).SelectByIndex(index);

        }

        public static void CloseAllSessions()
        {
            PropertiesCollection.driver.Close();
            // PropertiesCollection.driver.Quit();
        }
    }
}