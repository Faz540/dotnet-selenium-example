using OpenQA.Selenium;

namespace dotnet_selenium_example 
{
    class SeleniumGetMethods
    {
        public static bool IsDisplayed(string selector)
        {
            return PropertiesCollection.driver.FindElement(By.CssSelector(selector)).Displayed;
        }
        public static string GetText(string selector)
        {
            return PropertiesCollection.driver.FindElement(By.CssSelector(selector)).Text;
        }

        public static string GetURL()
        {
            return PropertiesCollection.driver.Url;
        }

        public static string GetPageTitle()
        {
            return PropertiesCollection.driver.Title;
        }

        public static bool IsSelected(string selector)
        {
            return PropertiesCollection.driver.FindElement(By.CssSelector(selector)).Selected;
        }

        public static string GetAttribute(string selector, string attribute)
        {
            return PropertiesCollection.driver.FindElement(By.CssSelector(selector)).GetAttribute(attribute);
        }
    }
}