using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace dotnet_selenium_example
{
    class WaitUntil
    {
        public static void elementIsDisplayed(IWebElement selector)
        {
            WebDriverWait wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(10));
            wait.Until(drvr => selector.Displayed);
        }

        public static void urlContains(string urlString)
        {
            WebDriverWait wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(10));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.UrlContains(urlString));
        }

        public static void urlEquals(string urlString)
        {
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(10));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.UrlMatches(urlString));
        }
    }
}