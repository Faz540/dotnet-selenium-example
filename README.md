## Author: Paul Farrell
- Having a cheeky gander at DotNet Core with Selenium WebDriver :O

### Prerequisites:
- [DotNet Core SDK for Visual Studio Core](https://dotnet.microsoft.com/download/dotnet-core/sdk-for-vs-code)
- The Google Chrome Browser.
- Probably VS Code *shrugs


### Installs all the project/repo depedencies:
```
dotnet build
```

### Run All The Tests:
```
dotnet test
```

-------------------------------------------------

## Notes For Me...In Case I Forget:

### Create a brand new .NET NUnit Project: (Similiar to ```npm init -y```)
```
    dotnet new nunit
```

### Install the latest version of Selenium WebDriver: (Similar to ```npm install selenium```)

```
    dotnet add package Selenium.WebDriver
```

### Install the latest version of ChromeDriver for Selenium WebDriver:
```
    dotnet add package Selenium.WebDriver.ChromeDriver
```

### Install some extra Selenium Stuff: (Handles Dropdown Menus better, Wait Helpers etc.)
```
    dotnet add package Selenium.Support
    dotnet add package DotNetSeleniumExtras.WaitHelpers
```
