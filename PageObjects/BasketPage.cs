using System.Collections.Generic;
using OpenQA.Selenium;

namespace dotnet_selenium_example
{
    class BasketPage
    {

        public IWebElement buttonProceedToCheckout => PropertiesCollection.driver.FindElement(By.CssSelector("#basket-checkout-btn-bottom"));
        public IList<IWebElement> basketItems => PropertiesCollection.driver.FindElements(By.CssSelector(".product-listing"));
        
        public void proceedToCheckout()
        {
            PropertiesCollection.driver.FindElements(By.CssSelector(".product-listing"));
            buttonProceedToCheckout.Click();
        }

        public int getNumberOfBasketItems() 
        {
            return basketItems.Count;
        }
    }
}