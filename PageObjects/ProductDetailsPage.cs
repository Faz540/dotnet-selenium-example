using OpenQA.Selenium;

namespace dotnet_selenium_example
{
    class ProductDetailsPage
    {
        public IWebElement buttonTopAddToBagButton => PropertiesCollection.driver.FindElement(By.CssSelector("a.add-to-basket-button"));
        public IWebElement buttonBottomAddToBagButton => PropertiesCollection.driver.FindElement(By.CssSelector("button.add-to-basket-button"));

        public void addToBag() 
        {
            buttonTopAddToBagButton.Click();
        }

        public void open(string url) {
            string baseURL = DotNetEnv.Env.GetString("BASEURL");
            SeleniumSetMethods.GoToURL(baseURL + url);
        }
    }
}