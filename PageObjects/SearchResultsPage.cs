using System.Collections.Generic;
using System.Collections.ObjectModel;
using OpenQA.Selenium;


namespace dotnet_selenium_example
{
    class SearchResultsPage
    {
        public IWebElement resultsCarousel => PropertiesCollection.driver.FindElement(By.CssSelector(".carousel-holder"));
        public IList<IWebElement> results => PropertiesCollection.driver.FindElements(By.CssSelector(".list-row-container"));
        public IList<IWebElement> buttonsAddToBag => PropertiesCollection.driver.FindElements(By.CssSelector(".add-to-basket-button"));
        
        public int getNumberOfResults()
        {
            return results.Count;
        }

        public void addItemToBasket(int index = 0)
        {
            buttonsAddToBag[index].Click();
        }
    }
}