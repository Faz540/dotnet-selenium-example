using System.Collections.Generic;
using OpenQA.Selenium;

namespace dotnet_selenium_example
{
    class ListerPage
    {
        public IList<IWebElement> productCards => PropertiesCollection.driver.FindElements(By.CssSelector("[data-test='desktop/plp/product-card']"));

        public int getNumberOfProductCards()
        {
            return productCards.Count;
        }
    }
}