using System;
using OpenQA.Selenium;

namespace dotnet_selenium_example 
{
    class Header
    {
        public IWebElement inputSearchField => PropertiesCollection.driver.FindElement(By.CssSelector("#srch-str"));
        public IWebElement buttonSearch => PropertiesCollection.driver.FindElement(By.CssSelector("#header-search-button"));
        public IWebElement textNumberOfBasketItems => PropertiesCollection.driver.FindElement(By.CssSelector("#basket-item-count"));
        public void searchFor(string searchTerm) 
        {
            inputSearchField.Clear();
            inputSearchField.SendKeys(searchTerm);
            buttonSearch.Click();
        }
        public int getNumberOfBasketItems()
        {
            return Int32.Parse(textNumberOfBasketItems.Text);
        }
    }
}