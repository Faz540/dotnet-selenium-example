using System;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;

namespace dotnet_selenium_example
{
    public class Tests
    {
        ProductDetailsPage productDetailsPage = new ProductDetailsPage();
        BasketPage basketPage = new BasketPage();
        Header header = new Header();
        SearchResultsPage searchResultsPage = new SearchResultsPage();
        ListerPage listerPage = new ListerPage();

        [OneTimeSetUp]
        public void Initialize()
        {
            PropertiesCollection.driver = new ChromeDriver();
            PropertiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            PropertiesCollection.driver.Manage().Window.Maximize(); // << Put this in a method
        }

        [SetUp]
        public void Home()
        {
            PropertiesCollection.driver.Manage().Cookies.DeleteAllCookies();
            DotNetEnv.Env.Load("../../../.env");
            string baseURL = DotNetEnv.Env.GetString("BASEURL");
            SeleniumSetMethods.GoToURL(baseURL);
        }

        [Test]
        public void SearchingForAColourReturnsResults()
        {
            header.searchFor("red");
            // Wait for url to not contain "&ref=h"
            Assert.Greater(searchResultsPage.getNumberOfResults(), 0);
        }

        [Test]
        public void SearchingForABrandRedirectsToABrandListerPage()
        {
            header.searchFor("Fender");
            WaitUntil.urlContains("/Manufacturer/Fender.html");
            Assert.Greater(listerPage.getNumberOfProductCards(), 0);
        }

        [Test]
        public void SearchingForAnInventoryIDReturnsResults()
        {
            header.searchFor("7022");
            Assert.Greater(searchResultsPage.getNumberOfResults(), 0);
        }

        [Test]
        public void UserCanAddToBag()
        {
            productDetailsPage.open("/g4m/5");
            WaitUntil.elementIsDisplayed(productDetailsPage.buttonTopAddToBagButton);
            productDetailsPage.addToBag();
            WaitUntil.elementIsDisplayed(basketPage.buttonProceedToCheckout);
            Assert.AreEqual(basketPage.getNumberOfBasketItems(), 1);
            Assert.AreEqual(header.getNumberOfBasketItems(), 1);
        }

        [OneTimeTearDown]
        public void CleanUp()
        {
            SeleniumSetMethods.CloseAllSessions();
        }

    }
    
}